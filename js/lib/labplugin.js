import {AppModel, AppView, version} from './index';
import {IJupyterWidgetRegistry} from '@jupyter-widgets/base';

export const appWidgetPlugin = {
  id: 'jsParcoursUrge:plugin',
  requires: [IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'jsParcoursUrge',
          version: version,
          exports: { AppModel, AppView }
      });
  },
  autoStart: true
};

export default appWidgetPlugin;
