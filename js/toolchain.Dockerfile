From node

COPY package.json  /

RUN apt-get update
RUN apt-get install -y pip
RUN pip install jupyter jupyterlab

ENV NODE_OPTIONS --openssl-legacy-provider

RUN yarn config set prefix /
WORKDIR /src

RUN yarn install
CMD ["/bin/bash", "launch_npm.sh"]
