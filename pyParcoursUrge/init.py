import ipywidgets as widgets
import sys
import os
import asyncio
import json
import logging
from traitlets import Unicode, List, Integer
from enum import IntEnum

import parcoursprog
from .ParcoursvisClient import *

from ._version import NPM_PACKAGE_RANGE

logging.basicConfig()
connected = set()
state_context = {}
dataDirectory = "tmp"

class ViewType(IntEnum):
    """The different view types the user can be in"""
    MAIN     = 0
    SEQUENCE = 1

class SequenceViewType(IntEnum):
    """The two different sequence view type"""
    BEFORE = 0
    AFTER  = 1

def str_to_DiseaseFilterMode(name):
    if(name == "both"):
        return parcoursprog.DiseaseFilterMode.BOTH
    elif(name == "yes"):
        return parcoursprog.DiseaseFilterMode.YES
    elif(name == "no"):
        return parcoursprog.DiseaseFilterMode.NO
    raise KeyError("Name " + name + " is not a filter DiseaseFilterMode value")

@widgets.register
class ParcoursUrge(widgets.DOMWidget):
    """The ParcoursUrge widget.
    Note that this widget can only be instantiate once due to how it handles the DOM"""
    # Name of the widget view class in front-end
    _view_name = Unicode('AppView').tag(sync=True)
    # Name of the widget model class in front-end
    _model_name = Unicode('AppModel').tag(sync=True)
    # Name of the front-end module containing widget view
    _view_module = Unicode('jsParcoursUrge').tag(sync=True)
    # Name of the front-end module containing widget model
    _model_module = Unicode('jsParcoursUrge').tag(sync=True)
    # Version of the front-end module containing widget view
    _view_module_version = Unicode(NPM_PACKAGE_RANGE).tag(sync=True)
    # Version of the front-end module containing widget model
    _model_module_version = Unicode(NPM_PACKAGE_RANGE).tag(sync=True)

    # The current selected sequence
    current_sequence = List(Unicode, []).tag(sync=True)
    # The nodes IDs on which the user zoomed in the "Sequence" view. Use SequenceViewType to read this list and ctx.currentContext to get the correct context associated to these IDs.
    sequence_view_zoomed_nodes_ids = List(Integer, [-1, -1]).tag(sync=True)
    # The node ID on which the user zoomed in the "Main" view. This value is only useful when the user is in the Main view type (see view_type). To be used with ctx.currentContext to get the correct context associated to this ID.
    main_view_zoomed_node_id = Integer(-1).tag(sync=True)
    # The view type the user is currently in.
    view_type = Integer(ViewType.MAIN).tag(sync=True)

    def __init__(self, dataDirectory="tmp"):
        super().__init__()
        assert os.path.isfile(os.path.join(dataDirectory, "data.bin"))
        assert os.path.isfile(os.path.join(dataDirectory, "data.map"))
        assert os.path.isfile(os.path.join(dataDirectory, "indi.bin"))
        print(f"# using data from directory {dataDirectory}")

        self.on_msg(self._msg_handler)
        self._ctx = ParcoursvisClient(dataDirectory)
    
    def _msg_handler(self, widget, message, buffers):
        try:
            data = json.loads(message['data'])
            action = data.get("action")
            if action == "node_tree":
                self.nodetreeHandler(data)
            elif action == "distribution":
                # print("distribution requested")
                self.distributionHandler(data)
            elif action == "comparison":
                self.comparisonHandler(data)
            else:
                logging.error(f"unsupported event: {data}")
        except Exception as e:
            logging.error(f"unsupported message... {e}")
            pass

    def nodetreeHandler(self, data):
        "Handler of a nodetree message"
        print("nodetreee_handler", data)
        ctx     = self._ctx.currentContext
        params  = data["params"]
        reset   = bool(data.get("reset", False))
        process = bool(data.get("process", True))

        # only change the values when reseting
        for param, value in params.items():
            if param == "reset":
                assert reset == bool(value)
            elif param == "duration_type":
                ctx.duration_type = value
            elif param == "duration":
                ctx.min_duration = value[0]
                ctx.max_duration = value[1]
            elif param == "age":
                ctx.min_age = value[0]
                ctx.max_age = value[1]
            elif param == "interruption_duration":
                ctx.interruption_duration = value
            elif param == "depth":
                ctx.max_depth = value
            elif param == "quantum":
                ctx.quantum = value
            elif param == "prune_threshold":
                ctx.prune_threshold = value
            elif param == "aggregate_coarse_level":
                ctx.aggregate_coarse_level = value
            elif param == "hidden_items":
                ctx.hidden_items = value
            elif param == "align_sequence":
                ctx.align_sequence = value
            elif param == "align_sequence_by":
                ctx.align_sequence_by = value
            elif param == "diseases":
                for disease in value:
                    try:
                        ctx.disease_filter[list(ctx.diseases).index(disease["name"])] = str_to_DiseaseFilterMode(disease["status"])
                    except:
                        logging.warning("Disease %s or status %s is not a valid key", disease["name"], disease["status"])
            else:
                logging.warning("invalid parameter %s: %s", param, value)

        if reset or process:
            ctx.process_next(reset)
            ctx.update_view_data()
            ret = ctx.print().strip()
            if ret == '':
                ret = 'null'
            else: #Add additional information. This is "hacky" but works well on well-formed JSON object
                ret = f'{ret[:-1]}, \"reset\": {"true" if reset else "false"}}}'
            self.send({'data': '{"type": "nodetree",' +
                              f'"event_id": {data["event_id"]},' +
                               '"data": '+ret+"}"})


    def distributionHandler(self, data):
        "Handler of a distribution message"
        print("distributionHandler", data)
        ctx = self._ctx.currentContext
        ret = {}
        node_id = int(data["node_id"])
        ret["age"]      = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.AGE, node_id))
        ret["duration"] = json.loads(ctx.print_histogram(parcoursprog.HistogramToPrint.DURATION, node_id))
        ret["disease"]  = json.loads(ctx.print_barchart(parcoursprog.BarchartToPrint.DISEASE, node_id))
        ret["nbValues"] = ctx.output_graph.nodes[node_id].count

        self.send({'data': json.dumps({
                               "type": "distribution",
                               "data": ret,
                               "event_id": data["event_id"]
                           })
        })

    def comparisonHandler(self, data):
        params = data["params"]
        if params["stop"] == True:
            self._ctx.stopComparison()

        ctx_index = params["ctx_index"]
        if ContextIndex(ctx_index) == ContextIndex.COMPARISON_CONTEXT and not self._ctx.comparisonContext:
            self._ctx.startComparison()
        self._ctx.currentContext = self._ctx.contexts[ctx_index]

    # Get an instance of ParcoursVisClient associated with this session
    ctx = property(lambda self: self._ctx)
